public class MissingNumber_268 {
    public static int missingNumber(int[] nums) {
        int x1 = nums[0];
        int x2 = 0;

        // XOR of the elements
        for(int i=1;i<nums.length;i++) {
            x1 ^= nums[i];
        }

        // XOR of the indexes
        for(int i=1;i<=nums.length;i++) {
            x2 ^= i;
        }

        // return xor of x1 and x2
        return (x1 ^ x2);
    }

    public static void main(String[] args) {
        System.out.println("Missing Number\n");

        int[] input1 = {3,0,1};
        System.out.println(missingNumber(input1));

        int[] input2 = {9,6,4,2,3,5,7,0,1};
        System.out.println(missingNumber(input2));
    }
}
