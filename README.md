Day 1 --> [09 December]

- Missing Number (https://leetcode.com/problems/missing-number/description/)
- Best Time to Buy and Sell Stock (https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)
- Contains Duplicate (https://leetcode.com/problems/contains-duplicate/description/)
- Single Number (https://leetcode.com/problems/single-number/description/)
- Linked List Cycle (https://leetcode.com/problems/linked-list-cycle/description/)
- Middle of the Linked List (https://leetcode.com/problems/middle-of-the-linked-list/description/)
- Reverse Linked List (https://leetcode.com/problems/reverse-linked-list/description/)
- Palindrome Linked List (https://leetcode.com/problems/palindrome-linked-list/description/)
- Remove Linked List Elements (https://leetcode.com/problems/remove-linked-list-elements/description/)