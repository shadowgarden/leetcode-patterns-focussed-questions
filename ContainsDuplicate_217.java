import java.util.HashSet;
public class ContainsDuplicate_217 {
    public static boolean containsDuplicate(int[] nums) {
        HashSet<Integer> hset = new HashSet<>();

        for(int num: nums) {
            if(hset.contains(num)) {
                return true;
            }
            hset.add(num);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println("217. Contains Duplicate\n");
        int[] input = {1,2,3,1};
        System.out.println(containsDuplicate(input));

        int[] input2 = {1,2,3};
        System.out.println(containsDuplicate(input2));
    }

}
