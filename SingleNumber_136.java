public class SingleNumber_136 {
    public static int singleNumber(int[] nums) {
        int xor = nums[0];

        for(int i=1;i<nums.length;i++) {
            xor ^= nums[i];
        }

        return xor;
    }

    public static void main(String[] args) {
        System.out.println("136. Single Number");

        int[] input = {2,2,1};
        System.out.println(singleNumber(input));

        int[] input2 = {2,1,1};
        System.out.println(singleNumber(input2));
    }
}
