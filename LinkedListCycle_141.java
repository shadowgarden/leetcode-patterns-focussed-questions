import common.ListNode;

public class LinkedListCycle_141 {
    public static boolean hasCycle(ListNode head) {
        ListNode s = head;
        ListNode f = head;

        while(f != null && f.next != null) {
            s = s.next;
            f = f.next.next;

            if(s == f) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println("141. Linked List Cycle\n");
        ListNode a = new ListNode(3);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(0);
        ListNode d = new ListNode(-4);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = b;

        System.out.println(hasCycle(a));
    }
}
