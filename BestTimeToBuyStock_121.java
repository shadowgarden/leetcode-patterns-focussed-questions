public class BestTimeToBuyStock_121 {
    public static int maxProfit(int[] prices) {
        int max_profit = 0;
        int left_day = 0;
        int right_day = 1;
        while(right_day < prices.length) {
            if(prices[left_day] < prices[right_day]) {
                max_profit = Math.max(max_profit, (prices[right_day] - prices[left_day]));
            } else {
                left_day = right_day;
            }
            right_day += 1;

        }
        return max_profit;
    }

    public static void main(String[] args) {
        System.out.println("Best Time To Buy Stock");
        int[] input1 = {7,1,5,3,6,4};
        System.out.println(maxProfit(input1));

        int[] input2 = {7,6,4,3,1};
        System.out.println(maxProfit(input2));
    }
}
