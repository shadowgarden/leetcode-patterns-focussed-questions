import common.ListNode;

public class PalindromeLL_234 {
    public static boolean isPalindrome(ListNode head) {
        // Find middle node of the LL
        ListNode curr = head;
        ListNode middle = middleNode(head);

        ListNode head2 = middle.next;
        middle.next = null;

        // Reverse the 2nd LL
        ListNode nhead = reverseList(head2);

        // Check for palindrome
        ListNode a = nhead;
        ListNode b = head;

        boolean isPalindrome = true;
        while(a != null && b != null) {
            if(a.val!=b.val) {
                isPalindrome = false;
            }
            a = a.next;
            b = b.next;
        }

        return isPalindrome;
    }

    public static ListNode middleNode(ListNode head) {
        ListNode slow = head;
        ListNode fast = head.next;

        while(fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode next = null;
        ListNode curr = head;

        while(curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }

    public static void main(String[] args) {
        System.out.println("234. Palindrome Linked List\n");
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(2);
        head.next.next.next = new ListNode(1);
        head.next.next.next.next = new ListNode(3);

        ListNode head2 = new ListNode(1);
        head2.next = new ListNode(2);
        head2.next.next = new ListNode(2);
        head2.next.next.next = new ListNode(1);

        System.out.println(isPalindrome(head));
        System.out.println(isPalindrome(head2));
    }
}
