import common.ListNode;

public class RemoveLinkList {
    public static ListNode removeElements(ListNode head, int val) {
        ListNode curr = head;
        ListNode new_head = new ListNode(-1);
        ListNode temp = new_head;

        while(curr != null) {
            ListNode next = curr.next;
            if(curr.val != val) {
                temp.next = curr;
                temp = temp.next;
                temp.next = null;
            }

            curr = next;
        }
        return new_head.next;
    }

    public static void printList(ListNode head) {
        ListNode curr = head;
        while(curr != null) {
            System.out.println(curr.val);
            curr = curr.next;
        }
    }

    public static void main(String[] args) {
        System.out.println("203. Remove Linked List Elements\n");
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(6);
        head.next.next.next = new ListNode(3);
        head.next.next.next.next = new ListNode(4);
        head.next.next.next.next.next = new ListNode(5);
        head.next.next.next.next.next.next = new ListNode(6);
        head.next.next.next.next.next.next.next = null;

        ListNode head2 = new ListNode(7);
        head2.next = new ListNode(7);
        head2.next.next = new ListNode(7);
        head2.next.next.next = new ListNode(7);
        head2.next.next.next.next = null;

        removeElements(head, 6);
        printList(head);
        System.out.println("************");

        removeElements(head2, 7);
        printList(head2);
    }
}
