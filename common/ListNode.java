package common;

/**
 * Definition for singly-linked list.
 * class common.ListNode {
 * int val;
 * common.ListNode next;
 * common.ListNode(int x) {
 * val = x;
 * next = null;
 * }
 * }
 */

public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
        next = null;
    }
}
